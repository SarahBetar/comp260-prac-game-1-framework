﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public Vector2 velocity;
	public float maxSpeed = 5.0f;
	private BeeSpawner beeSpawner;

	void Start() {
		// find the bee spawner and store a reference for later
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}


	public float destroyRadius = 1.0f; 

	void Update() {
		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal2");
		direction.y = Input.GetAxis("Vertical2");

		// scale by the maxSpeed parameter
		Vector2 velocity = direction.normalized * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}



}